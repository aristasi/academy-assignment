package com.domain;

import service.PartService;
import service.RepairService;
import service.UserService;
import service.VehicleService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {


        UserService userService = new UserService();
        RepairService repairService = new RepairService();
        VehicleService vehicleService = new VehicleService();
        PartService partService = new PartService();


        //USERS

        User user = new User(1, "aris@aris.gr", "newpass", "Aris", "Tasios", "Triantafyllidi 33", 133527469, "administrator");
        User user2 = new User(2, "dimitris@dimitris.gr", "pass", "Dimitris", "Tasios", "Thermopilon 52", 133527460, "user");
        User user3 = new User(3, "christos@dimitris.gr", "password", "Christos", "Tasios", "Thiatiron 82", 127333353, "user");
        User user4 = userService.createUser(4, "george@george.gr", "georgepassword", "George", "Tasios", "Chatzistavrou", 133527458, "user");


        //DATES OF CREATION

        LocalDate newd1 = LocalDate.of(2015, 1, 1);
        LocalDate newd2 = LocalDate.of(2016, 2, 2);
        LocalDate newd3 = LocalDate.of(2017, 3, 3);
        LocalDate newd4 = LocalDate.of(2010, 5, 3);
        LocalDate newd5 = LocalDate.of(2014, 8, 10);
        LocalDate newd6 = LocalDate.of(2018, 1, 2);


        //DATES OF REPAIRS

        LocalDate newRepairDate7 = LocalDate.of(2018, 10, 2);
        LocalDate newRepairDate8 = LocalDate.of(2019, 2, 1);
        LocalDate newRepairDate9 = LocalDate.of(2018, 5, 8);
        LocalDate newRepairDate10 = LocalDate.of(2019, 3, 5);
        LocalDate newRepairDate11 = LocalDate.of(2018, 9, 6);
        LocalDate newRepairDate12 = LocalDate.of(2019, 1, 3);
        LocalDate newRepairDate13 = LocalDate.of(2019, 2, 2);


        //VEHICLES

        Vehicle vehicleOne = new Vehicle(10, "Toyota", "Yaris", newd1, "Blue", 100.0, user, "AHK7058");
        Vehicle vehicleTwo = new Vehicle(20, "VW", "Polo", newd2, "Yellow", 200.0, user2, "AHM6689");
        Vehicle vehicleThree = new Vehicle(30, "BMW", "116", newd3, "Yellow", 300.0, user3, "AHE2002");
        Vehicle vehicleFour = new Vehicle(40, "MB", "AClass", newd4, "Black", 400.0, user, "AHK6651");
        Vehicle vehicleFive = new Vehicle(50, "Opel", "Astra", newd5, "Red", 500.0, user2, "AHE5325");
        Vehicle vehicleSix = new Vehicle(60, "AUDI", "A3", newd6, "White", 600.0, user3, "AHK2000");
        Vehicle vehicleSeven = new Vehicle(70, "Renault", "A3", newd6, "White", 600.0, user4, "AHM5462");
        Vehicle vehicleEight = vehicleService.createVehicle(80, "Pegeuot", "207", newd1, "Black", 200.0,"AHH5325");
        Vehicle vehicleNine = vehicleService.createVehicle(90, "Pegeuot", "307", newd1, "Black", 200.0, "AA4710");


        //PARTS

        Part engine = new Part(100, "Engine", "EngineType", 500.0);
        Part wheel = new Part(200, "Wheel", "WheelType", 100.0);
        Part brakes = new Part(300, "Brakes", "BrakeType", 150.0);
        Part gearBox = new Part(400, "GearBox", "GearboxType", 220.0);
        Part oilFilter = new Part(500, "OilFilter", "Filter", 10.0);
        Part airFilter = new Part(600, "airFilter", "Filter", 20.0);
        Part tyre = partService.createPart(700, "Tyre", "TyreType", 70.0);

        //REPAIRS

        Repair repairOne = new Repair(11, newRepairDate7, "ServiceOne", 150.0, vehicleOne);
        Repair repairTwo = new Repair(22, newRepairDate8, "ServiceTwo", 100.0, vehicleTwo);
        Repair repairThree = new Repair(33, newRepairDate9, "ServiceThree", 200.0, vehicleThree);
        Repair repairFour = new Repair(44, newRepairDate10, "ServiceFour", 250.0, vehicleFour);
        Repair repairFive = new Repair(55, newRepairDate11, "ServiceFive", 100.0, vehicleFive);
        Repair repairSix = new Repair(66, newRepairDate12, "ServiceSix", 300.0, vehicleSix);
        Repair repairSeven = new Repair(77, newRepairDate13, "ServiceSeven", 120.0, vehicleOne);
        Repair repairEight = new Repair(88, newRepairDate13, "ServiceEight", 111.0, vehicleEight);
        Repair repairNine = repairService.createRepair(99, newRepairDate7, "ServiceNine", 500.0, vehicleOne, new ArrayList<>());


        // ADD USERS TO LIST
        userService.createUser(user);
        userService.createUser(user2);
        userService.createUser(user3);

        //User user1 = userService.findAllUsers().get(0);


        //ADD VEHICLES TO USER
        user.addVehicleToList(vehicleOne);
        user.addVehicleToList(vehicleTwo);
        user2.addVehicleToList(vehicleThree);
        user2.addVehicleToList(vehicleFour);
        user3.addVehicleToList(vehicleFive);
        user3.addVehicleToList(vehicleSix);
        user4.addVehicleToList(vehicleSeven);
        user4.addVehicleToList(vehicleEight);
        user.addVehicleToList(vehicleNine);


        // ADD PARTS TO REPAIR
        repairOne.addPartToList(engine);
        repairOne.addPartToList(wheel);
        repairTwo.addPartToList(brakes);
        repairThree.addPartToList(gearBox);
        repairFour.addPartToList(oilFilter);
        repairFive.addPartToList(airFilter);
        repairSix.addPartToList(engine);
        repairSeven.addPartToList(wheel);
        repairSeven.addPartToList(oilFilter);
        repairNine.addPartToList(oilFilter);
        repairNine.addPartToList(tyre);

        //ADD REPAIRS TO LIST
        repairService.createRepair(repairOne);
        repairService.createRepair(repairTwo);
        repairService.createRepair(repairThree);
        repairService.createRepair(repairFour);
        repairService.createRepair(repairFive);
        repairService.createRepair(repairSix);
        repairService.createRepair(repairSeven);

        //VEHICLE DAO LIST
        vehicleService.createVehicle(vehicleOne);
        vehicleService.createVehicle(vehicleTwo);
        vehicleService.createVehicle(vehicleThree);
        vehicleService.createVehicle(vehicleFour);
        vehicleService.createVehicle(vehicleFive);
        vehicleService.createVehicle(vehicleSix);
        vehicleService.createVehicle(vehicleSeven);


        //ADD PARTS TO DAO
        partService.createPart(engine);
        partService.createPart(wheel);
        partService.createPart(brakes);
        partService.createPart(gearBox);
        partService.createPart(oilFilter);
        partService.createPart(airFilter);


        // USERS FUNCTIONALITY TESTING
        //userService.findAllUsers();
        //List<User> listOfUsers = userService.findAllUsers();
        //for (User us : listOfUsers) {
        //    System.out.println(us);
        //}
        //userService.searchForUserByEmail("aris@aris.gr");
        //userService.updateUserById(1,user2);
        //userService.findAllUsers();
        //boolean flag =userService.deleteUser(1);
        //System.out.println(flag);

//        for (User us :
//                listOfUsers) {
//            System.out.println(us);
//
//        }

        //User userk = userService.searchForUserById(3);
        //System.out.println(userk);
        //User userTest = userService.searchForUserByEmail("dimitris@dimitris.gr");
        //System.out.println(userTest);
        List<Vehicle> vehiclesOfAUser = userService.findAllVehiclesOfAUser(1);
        for (Vehicle vehicle :
                vehiclesOfAUser) {
            System.out.println(vehicle);
        }
        //System.out.println(repairNine);
//        List<Repair> repairsOfAUser = userService.findAllRepairsOfAllVehiclesOfASpecificUser(1);
//        for (Repair repair :
//                repairsOfAUser) {
//            System.out.println(repair);
//        }
        //userService.loginUser("aris@aris.gr","newpass");
        //userService.findAllVehiclesOfAUser(1);
        List<Repair> rep = userService.findAllRepairsOfAllVehiclesOfASpecificUser(1);
        for (Repair reps :
                rep) {
            System.out.println(reps);
        }
//        userService.findAllUsers();

        // VEHICLE FUNCTIONALITY TESTING
//        List<Vehicle> listOfVehicles = vehicleService.findAllVehicles();
//        for (Vehicle vehicle :listOfVehicles) {
//            System.out.println(vehicle);
//        }
//        vehicleService.deleteVehicle(70);
//        List<Vehicle> listOfVehicles = vehicleService.findAllVehicles();
//        for (Vehicle vehicle :listOfVehicles) {
//            System.out.println(vehicle);
//        }

//        boolean b =vehicleService.updateVehicle(10, vehicleEight);
//        System.out.println(b);
//        Vehicle v = vehicleService.searchForVehicleByPlateNumber("AHK7058");
//        System.out.println(v);
//        List<Repair> listOfRepairs = vehicleService.findAllRepairsOfAVehicle(10);
//        System.out.println(listOfRepairs);

        // REPAIR FUNCTIONALITY TESTING
        //boolean bb= repairService.updateRepairById(11, repairEight);
        //System.out.println(bb);
//       List<Repair> NewList = repairService.findAllRepairs();
//        for (Repair repair :
//                NewList) {
//            System.out.println(repair);
//        }
//        List<Repair> newlist1 = repairService.searchForRepairByVehicle(vehicleOne);
//        for (Repair repair :
//                newlist1) {
//            System.out.println(repair);
//        }
//        Repair rep =repairService.searchForRepairById(77);
//        System.out.println(rep);
//        repairService.calculateTotalCostOfARepair(repairSeven);

            // PART FUNCTIONALITY TESTING
            //boolean p =partService.updatePartById(100, tyre);
            //System.out.println(p);
//        List<Part> listOfParts =partService.findAllParts();
//        for (Part part :
//                listOfParts) {
//            System.out.println(part);
//
//        }
//        Part search = partService.searchForPartById(200);
//        System.out.println(search);
//        Part searchType =partService.searchForPartByType("EngineType");
//        System.out.println(searchType);

        }
    }

