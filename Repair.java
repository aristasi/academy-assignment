package com.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Repair {

    private long id;
    private LocalDate date;
    private String status;
    private double cost;
    private Vehicle vehicle;
    private List<Part> parts;

    public Repair(long id, LocalDate date, String status, double cost, Vehicle vehicle, List<Part> parts) {
        this.id = id;
        this.date = date;
        this.status = status;
        this.cost = cost;
        this.vehicle = vehicle;
        this.parts = parts;
    }

    public Repair(long id, LocalDate date, String status, double cost, Vehicle vehicle) {
        this.id = id;
        this.date = date;
        this.status = status;
        this.cost = cost;
        this.vehicle = vehicle;
        this.parts = new ArrayList<>();
    }

    public List<Part> getParts() {
        return parts;
    }

    public void setParts(List<Part> parts) {
        this.parts = parts;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void addPartToList(Part part){
        this.parts.add(part);
    }



    @Override
    public String toString() {
        return "Repair{" +
                "id=" + id +
                ", date=" + date +
                ", status='" + status + '\'' +
                ", cost=" + cost +
                ", vehicle=" + vehicle.getBrand() +
                ", parts=" + parts +
                '}';
    }
}
